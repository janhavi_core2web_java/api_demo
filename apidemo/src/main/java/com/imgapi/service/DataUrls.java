package com.imgapi.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DataUrls {
    
    public StringBuffer getresponseData()throws IOException{


        String url = "https://api.unsplash.com/photos/random/?client_id=zx7A0K1TyyU87qd5t4n69czA7kjyrRLdm4sKsxYiCbw";

        HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();

        httpClient.setRequestMethod("GET");

        StringBuffer response = new StringBuffer();

        int responseCode = httpClient.getResponseCode();

        if(responseCode == HttpURLConnection.HTTP_OK){

            BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()));

            String inputline;

            while ((inputline = in.readLine())!=null) {
                
                response.append(inputline);
            }

            in.close();
            return response;


        }
        else{

            throw new RuntimeException("GET Request Failed with Response Code" + responseCode);

        }
    }
}
