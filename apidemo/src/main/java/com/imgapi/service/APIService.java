package com.imgapi.service;

import java.io.IOException;

import org.json.JSONObject;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class APIService extends Application {
        static String imgUrl = "";

        public static void imageData() throws IOException{

            StringBuffer response = new DataUrls().getresponseData();

            if(response != null){

                JSONObject obj = new JSONObject(response.toString());
                JSONObject urlObject = obj.getJSONObject("urls");

                imgUrl = urlObject.getString("small");
            }
            else{
                System.out.println("response is empty");
            }
            
            
        }

        @Override
        public void start(Stage primaryStage) throws Exception {
        
            imageData();
            Image ig = new Image(imgUrl);

            ImageView iv = new ImageView(ig);
            
            Pane imgPane = new Pane();
            imgPane.getChildren().add(iv);

            Scene sc = new Scene(imgPane, ig.getWidth(),ig.getHeight());
            primaryStage.setScene(sc);

            primaryStage.setTitle("MyAPI Image");
            primaryStage.show();
        }
}
